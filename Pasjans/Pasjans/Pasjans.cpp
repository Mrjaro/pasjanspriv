// Pasjans.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Game.h"
#include "Menu.h"

int _tmain(int argc, _TCHAR* argv[])
{
	Menu M;
	game G;

	for (;;){
		M.Display();
		G.Init();
		G.Loop(true);
	}

	return 0;
}

