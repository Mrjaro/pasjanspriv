#include "stdafx.h"
#include "Talia.h"
#include "Piramida.h"

void piramida::Init()
{
	for (int i = 0; i < 3; i++)
	{
		t1[i] = new karta;
	}
	for (int i = 0; i < 6; i++)
	{
		t2[i] = new karta;
	}
	for (int i = 0; i < 9; i++)
	{
		t3[i] = new karta;
	}
	for (int i = 0; i < 10; i++)
	{
		t4[i] = new karta;
	}
}

void piramida::Disp()
{
	for (int i = 0; i<3; i++)
	{
		if (t1[i] != NULL)
		{
			if (t2[i * 2] == NULL && t2[i * 2 + 1] == NULL)
			{
				WyswietlKarteXY(t1[i]->numer, t1[i]->kolor, (i * 18) + 10, 0);
				t1[i]->czyOdkryta = true;
			}
			else
			{
				WyswietlKarteXY(0, 0, (i * 18) + 10, 0);
			}
		}
	}
	for (int i = 0; i<6; i++)
	{
		if (t2[i] != NULL)
		{
			if (i % 2 == 0)
			{
				if (t3[static_cast<int>(i*1.5)] == NULL && t3[static_cast<int>(i*1.5) + 1] == NULL)
				{
					WyswietlKarteXY(t2[i]->numer, t2[i]->kolor, (i * 9) + 7, 3);
					t2[i]->czyOdkryta = true;
				}
				else
				{
					WyswietlKarteXY(0, 0, (i * 9) + 7, 3);
				}
			}
			else
			{
				if (t3[i + (i / 2)] == NULL && t3[i + (i / 2) + 1] == NULL)
				{
					WyswietlKarteXY(t2[i]->numer, t2[i]->kolor, (i * 9) + 4, 3);
					t2[i]->czyOdkryta = true;
				}
				else
				{
					WyswietlKarteXY(0, 0, (i * 9) + 4, 3);
				}
			}
		}
	}
	for (int i = 0; i<9; i++)
	{
		if (t3[i] != NULL)
		{
			if (t4[i] == NULL && t4[i + 1] == NULL)
			{
				WyswietlKarteXY(t3[i]->numer, t3[i]->kolor, (i * 6) + 4, 6);
				t3[i]->czyOdkryta = true;
			}
			else
			{
				WyswietlKarteXY(0, 0, (i * 6) + 4, 6);
			}
		}
	}
	for (int i = 0; i<10; i++)
	{
		if (t4[i] != NULL)
		{
			WyswietlKarteXY(t4[i]->numer, t2[i]->kolor, (i * 6) + 1, 9);
			t4[i]->czyOdkryta = true;
		}
	}
}

void piramida::Delete()
{
	for (int i = 0; i<3; i++)
	{
		delete t1[i];
	}
	for (int i = 0; i<6; i++)
	{
		delete t2[i];
	}
	for (int i = 0; i<9; i++)
	{
		delete t3[i];
	}
	for (int i = 0; i<10; i++)
	{
		delete t4[i];
	}
}

karta* piramida::VisualPointer(int row, int col)
{
	int a = col;
	int b = row;
	switch (row)
	{
	case 0:
		col = (col * 18) + 9;
		break;
	case 1:
		if (col % 2 == 0)
			col = (col * 9) + 6;
		else
			col = (col * 9) + 3;
		break;
	case 2:
		col = (col * 6) + 3;
		break;
	case 3:
		col = col * 6;
		break;
	}
	row = row * 3;

	GoToXY(col, row);
	cout << ">";
	GoToXY(col, row + 1);
	cout << ">";
	GoToXY(col, row + 2);
	cout << ">";
	GoToXY(col + 6, row);
	cout << "<";
	GoToXY(col + 6, row + 1);
	cout << "<";
	GoToXY(col + 6, row + 2);
	cout << "<";

	GoToXY(0, 14);
	switch (b)
	{
	case 0:
		return t1[a];
		//cout << t1[a]->kolor << " " << t1[a]->numer << endl;
		break;
	case 1:
		return t2[a];
		//cout << t2[a]->kolor << " " << t2[a]->numer << endl;
		break;
	case 2:
		return t3[a];
		//cout << t3[a]->kolor << " " << t3[a]->numer << endl;
		break;
	case 3:
		return t4[a];
		//cout << t4[a]->kolor << " " << t4[a]->numer << endl;
		break;
	}
}

karta* piramida::GetCard(int nrTabl, int index)
{
	switch (nrTabl)
	{
	case 1:
		return t1[index];
		break;
	case 2:
		return t2[index];
		break;
	case 3:
		return t3[index];
		break;
	case 4:
		return t4[index];
		break;
	}
}

