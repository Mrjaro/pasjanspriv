#include "stdafx.h"
#include "Menu.h"
#include <iostream>
#include <conio.h>
using namespace std;


void Menu::Display()
{
	MainMenu();
}

void Menu::MainMenu()
{
	system("cls");

	isMenu = true;

	cout << "  MAGICZNE WIERZE " << endl
		<< "//////////////////" << endl
		<< "//    1.GRAJ!   //" << endl
		<< "//    2.TWORCY  //" << endl
		<< "//    3.POMOC   //" << endl
		<< "//////////////////" << endl;
	while (isMenu)
	{
		char key = _getch();
		switch (key)
		{
		case '1':
			isMenu = false;
			break;

		case '2':
			Credits();
			break;

		case '3':
			Help();
			break;

		case 27:
			exit(0);

		}
	}
}

void Menu::Credits()
{
	system("cls");

	cout << "Tworcy to:\n"
		<< "Wojciech Englart\n"
		<< "Jakob bronowski\n"
		<< "Jaroslaw Majtyka\n\n"
		<< "Nacisnij spacje by wrocic.\n";

	if (_getch() == 32)
		MainMenu();
	else
		Credits();
	
}

void Menu::Help()
{
	system("cls");

	cout << "Zasady:\n"
		<< "Musimy nalozyc wszystkie karty z piramid na tali na dole.\n"
		<< "jednak karta ktora nakladamy musi byc albo o 1 mniejsza albo o 1 wiekrza\n"
		<< "od kart na ktora ja nakladamy. Jezeli nie pasuje nam rzadna karta to\n"
		<< "mozemy usunac karte z gory tali, jednak wtedy pozbywamu sie jej nieodwracalnie.\n"
		<< "Jezeli usuniemy wszystkie karty z piramid - wygrywamy.\n"
		<< "Jezeli usuniemy wszystkie karty z talii przegrywamy.\n\n";

	cout << "Sterowanie:\n"
		<< "W, S, A, D - porusznie kursorem\n"
		<< "X - nastepna karta z talii\n"
		<< "SPACJA - wybieranie kart\n\n"
		<< "Nacisnij spacje by wrocic.\n";
	
	
	if (_getch() == 32)
		MainMenu();
	else
		Help();
	
}