#pragma once

class Menu
{
public:
	void Display();

private:
	void MainMenu();
	void Credits();
	void Help();

	bool isMenu;
};

