#pragma once

#include "Piramida.h"

class game
{
private:
	piramida P;
	talia T;

public:
	void Init();
	void Loop(bool isPlating);
	void Controls(int &a, int &b, bool &isPlaying);
	void Warunek(bool &isPlaying);
	void Clean();
};