//All rights reserved by Wojciech Englart

struct game
{
    piramida P;
    talia T;
    void Init();
    void Loop();
    void Controls(int &a, int &b);
};

void game::Init()
{
    karta wsk;
    P.Init();
    T.StworzTalie();
    T.Tasuj();
    for(int i = 0; i<3; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t1[i]->kolor = wsk.kolor;
        P.t1[i]->numer = wsk.numer;
    }
    for(int i = 0; i<6; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t2[i]->kolor = wsk.kolor;
        P.t2[i]->numer = wsk.numer;
    }
    for(int i = 0; i<10; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t4[i]->kolor = wsk.kolor;
        P.t4[i]->numer = wsk.numer;
    }
    for(int i = 0; i<9; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t3[i]->kolor = wsk.kolor;
        P.t3[i]->numer = wsk.numer;
    }
}

void game::Loop()
{
    int a,b;
    a = 3;
    b = 0;
    for(;;){
        system("cls");
        P.Disp();
        P.VisualPointer(a, b);
        T.VisualDisp();
        this->Controls(a, b);
    }
}

void game::Controls(int &a, int &b)
{
    if(kbhit)
        {
            char key = getch();
            switch(key){
            case 'a':
                b--;
                break;

            case 'd':
                b++;
                break;

            case 'w':
                a--;
                break;

            case 's':
                a++;
                break;
            case 32:
                int nrKZT;
                nrKZT = T.tail->card.numer;

                switch(a)
                {
                case 0:
                    if(P.t1[b]->numer == nrKZT+1 || P.t1[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t1[b]);
                        //delete P.t1[b];
                        P.t1[b] = NULL;
                    }
                    else
                    {
                        if( P.t1[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t1[b]);
                            //delete P.t1[b];
                            P.t1[b] = NULL;
                        }
                        if( P.t1[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t1[b]);
                            //delete P.t1[b];
                            P.t1[b] = NULL;
                        }
                    }
                    //delete P.t1[b];
                    //P.t1[b] = NULL;
                    //cout << t1[a]->kolor << " " << t1[a]->numer << endl;
                    break;
                case 1:
                    if(P.t2[b]->numer == nrKZT+1 || P.t2[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t2[b]);
                        //delete P.t1[b];
                        P.t2[b] = NULL;
                    }
                    else
                    {
                        if( P.t2[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t2[b]);
                            //delete P.t1[b];
                            P.t2[b] = NULL;
                        }
                        if( P.t2[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t2[b]);
                            //delete P.t1[b];
                            P.t2[b] = NULL;
                        }
                    }
                    //cout << t2[a]->kolor << " " << t2[a]->numer << endl;
                    break;
                case 2:
                    if(P.t3[b]->numer == nrKZT+1 || P.t3[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t3[b]);
                        //delete P.t1[b];
                        P.t3[b] = NULL;
                    }
                    else
                    {
                        if( P.t3[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t3[b]);
                            //delete P.t1[b];
                            P.t3[b] = NULL;
                        }
                        if( P.t3[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t3[b]);
                            //delete P.t1[b];
                            P.t3[b] = NULL;
                        }
                    }
                    //cout << t3[a]->kolor << " " << t3[a]->numer << endl;
                    break;
                case 3:
                    if(P.t4[b]->numer == nrKZT+1 || P.t4[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t4[b]);
                        //delete P.t1[b];
                        P.t4[b] = NULL;
                    }
                    else
                    {
                        if( P.t4[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t4[b]);
                            //delete P.t1[b];
                            P.t4[b] = NULL;
                        }
                        if( P.t4[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t4[b]);
                            //delete P.t1[b];
                            P.t4[b] = NULL;
                        }
                    }
                    //cout << t4[a]->kolor << " " << t4[a]->numer << endl;
                    break;
                }
                break;

            case 'x':
                delete T.WydajOstat();
                break;
            }
        }
}
