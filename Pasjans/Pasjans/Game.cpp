#include "stdafx.h"
#include "Game.h"

void game::Init()
{
	karta wsk;
	P.Init();
	T.StworzTalie();
	T.Tasuj();
	for (int i = 0; i<3; i++)
	{
		wsk = T.Wydaj(i)->card;
		P.t1[i]->kolor = wsk.kolor;
		P.t1[i]->numer = wsk.numer;
		P.t1[i]->czyOdkryta = false;
	}
	for (int i = 0; i<6; i++)
	{
		wsk = T.Wydaj(i)->card;
		P.t2[i]->kolor = wsk.kolor;
		P.t2[i]->numer = wsk.numer;
		P.t2[i]->czyOdkryta = false;
	}
	for (int i = 0; i<9; i++)
	{
		wsk = T.Wydaj(i)->card;
		P.t3[i]->kolor = wsk.kolor;
		P.t3[i]->numer = wsk.numer;
		P.t3[i]->czyOdkryta = false;
	}
	for (int i = 0; i<10; i++)
	{
		wsk = T.Wydaj(i)->card;
		P.t4[i]->kolor = wsk.kolor;
		P.t4[i]->numer = wsk.numer;
		P.t4[i]->czyOdkryta = true;
	}
}

void game::Loop(bool isPlaying)
{
	int a, b;
	a = 3;
	b = 0;
	while (isPlaying)
	{
		system("cls");
		GoToXY(0, 16);
		//cout << "Wyczyszczono";

		//cout << " --Zaczynam Disp";
		P.Disp();
		GoToXY(0, 17);
		//cout << "Wyswietlono";

		P.VisualPointer(a, b);
		GoToXY(0, 18);
		//cout << "Wskaznik";

		T.VisualDisp();
		GoToXY(0, 19);
		//cout << "Talia";

		this->Warunek(isPlaying);
		this->Controls(a, b, isPlaying);
	};
}

void game::Controls(int &y, int &x, bool &isPlaying)
{
	if (_kbhit)
	{
		char key = _getch();
		switch (key){
		case 'a':
			x--;
			break;

		case 'd':
			x++;
			break;

		case 'w':
			y--;
			break;

		case 's':
			y++;
			break;

		case 27:
			isPlaying = false;
			break;

		case 32:
			int nrKZT;
			nrKZT = T.GetTail()->card.numer;

			switch (y)
			{
			case 0:
				if (P.t1[x] != NULL && P.t1[x]->czyOdkryta)
				{
					if (P.t1[x]->numer == nrKZT + 1 || P.t1[x]->numer == nrKZT - 1)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t1[x]);
						P.t1[x] = NULL;
					}
					else if (P.t1[x]->numer == 13 && nrKZT == 1)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t1[x]);
						P.t1[x] = NULL;
					}
					else if (P.t1[x]->numer == 1 && nrKZT == 13)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t1[x]);
						P.t1[x] = NULL;
					}
				}
				break;
			case 1:if (P.t2[x] != NULL && P.t2[x]->czyOdkryta)
			{
					   if (P.t2[x]->numer == nrKZT + 1 || P.t2[x]->numer == nrKZT - 1)
					   {
						   delete T.WydajOstat();
						   T.DopiszKarte(P.t2[x]);
						   P.t2[x] = NULL;
					   }
					   else if (P.t2[x]->numer == 13 && nrKZT == 1)
					   {
						   delete T.WydajOstat();
						   T.DopiszKarte(P.t2[x]);
						   P.t2[x] = NULL;
					   }
					   else if (P.t2[x]->numer == 1 && nrKZT == 13)
					   {
						   delete T.WydajOstat();
						   T.DopiszKarte(P.t2[x]);
						   P.t2[x] = NULL;
					   }
			}
				   break;
			case 2:
				if (P.t3[x] != NULL && P.t3[x]->czyOdkryta)
				{
					if (P.t3[x]->numer == nrKZT + 1 || P.t3[x]->numer == nrKZT - 1)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t3[x]);
						P.t3[x] = NULL;
					}
					else if (P.t3[x]->numer == 13 && nrKZT == 1)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t3[x]);
						P.t3[x] = NULL;
					}
					else if (P.t3[x]->numer == 1 && nrKZT == 13)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t3[x]);
						P.t3[x] = NULL;
					}
				}
				break;
			case 3:
				if (P.t4[x] != NULL && P.t4[x]->czyOdkryta){
					if (P.t4[x]->numer == nrKZT + 1 || P.t4[x]->numer == nrKZT - 1)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t4[x]);
						P.t4[x] = NULL;
					}
					else if (P.t4[x]->numer == 1 && nrKZT == 13)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t4[x]);
						P.t4[x] = NULL;
					}
					else if (P.t4[x]->numer == 13 && nrKZT == 1)
					{
						delete T.WydajOstat();
						T.DopiszKarte(P.t4[x]);
						P.t4[x] = NULL;
					}
				}
				break;
			}
			break;

		case 'x':
			delete T.WydajOstat();
			break;
		}
	}
	if (x < 0) x = 0;
	if (y < 0) y = 0;
	if (y > 3) y = 3;
	if (y == 3 && x > 9) x = 9;
	if (y == 2 && x > 8) x = 8;
	if (y == 1 && x > 5) x = 5;
	if (y == 0 && x > 2) x = 2;
}

void game::Warunek(bool &isPlaying)
{
	int count = 0;
	taliaKarta *wsk;
	wsk = T.GetHead();
	int num = 0;
	while (NULL != wsk->next)
	{
		num++;
		wsk = wsk->next;
	};

	if (num == 0)
	{
		isPlaying = false;
		cout << "                            PRZEGRALES :(\n\n"
			<< "Ale nie matrw sie, nastepnym razem bedzie lepiej :)";
	}
		

	for (int i = 0; i < 3; i++)
	{
		if (P.t1[i] != NULL)
			count++;
	}
	for (int i = 0; i < 6; i++)
	{
		if (P.t2[i] != NULL)
			count++;
	}
	for (int i = 0; i < 9; i++)
	{
		if (P.t3[i] != NULL)
			count++;
	}
	for (int i = 0; i < 10; i++)
	{
		if (P.t4[i] != NULL)
			count++;
	}

	if (count == 0)
	{
		system("cls");
		cout << "                             WYGRALES!!!";
	}
}

void game::Clean()
{
	P.Delete();
	T.Delete();
}