#pragma once
#include "Talia.h"

struct piramida{
	karta *t1[3];
	karta *t2[6];
	karta *t3[9];
	karta *t4[10];
	void Init();
	void Disp();
	void Delete();
	void Controls(int &a, int &b);
	karta* VisualPointer(int, int);
	karta* GetCard(int nrTabl, int index);
};

//